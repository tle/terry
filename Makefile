CXX			= g++
CXXFLAGS    =
# Windows
OPENGL		= -lopengl32 -lglu32
SFML		= -lsfml-graphics -lsfml-window -lsfml-system 
SHELL       = C:/Windows/System32/cmd.exe
LIBS		= $(SFML) $(OPENGL)
# Linux
# LIBS		= -lglfw -libglu -libgl
INCLUDES	= -I./include

CFLAGS		= $(CXXFLAGS) $(INCLUDES)
LFLAGS		= $(CXXFLAGS)

TARGET		= bin/terry

OBJS		= objs/GameStateManager.o objs/main.o objs/MenuState.o objs/MainState.o

MKDIR_P		= mkdir -pf
.PHONY		= directories
OUT_DIR		= bin objs
directories	= $(OUT_DIR)

all:	directories $(TARGET)

directories:	$(OUT_DIR)

$(OUT_DIR):
	$(MKDIR_P) $(OUT_DIR)

$(TARGET): $(OBJS)
	$(CXX) $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)

objs/main.o: src/main.cpp
	$(CXX) $(CFLAGS) -c src/main.cpp -o objs/main.o

objs/GameStateManager.o: src/GameStateManager.cpp
	$(CXX) $(CFLAGS) -c src/GameStateManager.cpp -o objs/GameStateManager.o

objs/MenuState.o: src/MenuState.cpp
	$(CXX) $(CFLAGS) -c src/MenuState.cpp -o objs/MenuState.o

objs/MainState.o: src/MainState.cpp
	$(CXX) $(CFLAGS) -c src/MainState.cpp -o objs/MainState.o

		
clean:
	rm -f $(OBJS)
	rm -f $(TARGET)