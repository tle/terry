#ifndef GSM_H
#define GSM_H

#include "MainState.h"
#include "MenuState.h"
#include <map>
#include <string>

// SFML dependencies
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace std;

class GameStateManager {
	private:
		State* currentState;
		map<string, State *> gameStates;
		sf::RenderWindow *window;

		// helper functions
		void initStates();
		void initGraphics();

	public:
		GameStateManager() {};
		~GameStateManager() {};
		void init();
		void mainloop();
};
#endif