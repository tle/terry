#ifndef MAINSTATE_H
#define MAINSTATE_H

#include "State.h"
#include "GameStateManager.h"

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;


class MainState : public State {
	private:
		Clock mainClock;
	public:
		MainState() {
			mainClock = Clock();
		};
		~MainState() {};
		string run(RenderWindow *w);
};

#endif