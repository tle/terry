#ifndef MENUSTATE_H
#define MENUSTATE_H

#include "State.h"
#include "GameStateManager.h"

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>

using namespace std;

class MenuState : public State {
	private:
		sf::Clock menuClock;
	public:
		MenuState() {
			menuClock = sf::Clock();
		};
		~MenuState() {};

		string run(RenderWindow *w);
};

#endif