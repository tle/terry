#ifndef STATE_H
#define STATE_H

#include <string>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

// Forward declaration to avoid circular dependency
class State {
	public:
		State() {};
		~State() {};

		// Returns the next state after each iteration
		virtual string run(RenderWindow *w) = 0;
};

#endif