#ifndef STATE_DATA
#define STATE_DATA

#include <map>
#include <string>

#include "MenuState.h"
#include "MainState.h"

using namespace std;

class StateData {
	private:
		State *currentState;
		std::map<string, State *> gameStates;
	public:
		StateData() {
			gameStates["main"] = new MainState();
			gameStates["menu"] = new MenuState();
		}

		~StateData() {
			// State list iterator
			map<string, State *>::iterator s;
			for (s.begin(); s.end(); s++) {
				delete s;
			}
		}
};



#endif