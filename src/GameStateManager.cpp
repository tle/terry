#include "GameStateManager.h"

using namespace sf;

void GameStateManager::initStates() {
	MenuState *menu = new MenuState();
	MainState *main = new MainState();

	gameStates["menu"] = menu;
	gameStates["main"] = main;

	// Initialize the first game state to be the Menu State
	currentState = gameStates["menu"];
}

void GameStateManager::initGraphics() {	
	// Initialize the window
	window = new RenderWindow(VideoMode(800, 600, 32), "SFML OpenGL");
	//window = new Window(VideoMode(800, 600, 32), "Terry");
 
    // Set color and depth clear value
    glClearDepth(1.f);
    glClearColor(0.f, 0.f, 0.f, 0.f);

    // Enable Z-buffer read and write
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);

    // Setup a perspective projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90.f, 1.f, 1.f, 500.f);

    // TODO: GET THIS OUT
    window->PreserveOpenGLStates(true);
}

void GameStateManager::init() {
	initStates();
	initGraphics();
}

void GameStateManager::mainloop() {
	bool running = true;
	while (window->IsOpened()) {
		string nextState = currentState->run(window);
		currentState = gameStates[nextState];
	}
}