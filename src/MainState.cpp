#include "MainState.h"

string MainState::run(RenderWindow *w) {
	cout << "I'm in a MainState!" << endl;
	sf::Event Event;

	string nextState = "main";
	
	while (w->GetEvent(Event))	{
	    // Close window : exit
	    if (Event.Type == sf::Event::Closed)
	        w->Close();
	    // Escape key : exit
	    if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::Escape))
	        w->Close();

	    // S key : switch states
	    if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::S))
	        nextState = "menu";

	    // Resize event : adjust viewport
	    if (Event.Type == sf::Event::Resized)
	        glViewport(0, 0, Event.Size.Width, Event.Size.Height);
	}

	w->Clear();
	// Clear color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Apply some transformations
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	w->SetActive();

	glTranslatef(0.f, 0.f, -200.f);
	glRotatef(mainClock.GetElapsedTime() * 50, 1.f, 0.f, 0.f);
	glRotatef(mainClock.GetElapsedTime() * 30, 0.f, 1.f, 0.f);
	glRotatef(mainClock.GetElapsedTime() * 90, 0.f, 0.f, 1.f);

	// Draw a cube
	glBegin(GL_QUADS);

	    glVertex3f(-50.f, -50.f, -50.f);
	    glVertex3f(-50.f,  50.f, -50.f);
	    glVertex3f( 50.f,  50.f, -50.f);
	    glVertex3f( 50.f, -50.f, -50.f);

	    glVertex3f(-50.f, -50.f, 50.f);
	    glVertex3f(-50.f,  50.f, 50.f);
	    glVertex3f( 50.f,  50.f, 50.f);
	    glVertex3f( 50.f, -50.f, 50.f);

	    glVertex3f(-50.f, -50.f, -50.f);
	    glVertex3f(-50.f,  50.f, -50.f);
	    glVertex3f(-50.f,  50.f,  50.f);
	    glVertex3f(-50.f, -50.f,  50.f);

	    glVertex3f(50.f, -50.f, -50.f);
	    glVertex3f(50.f,  50.f, -50.f);
	    glVertex3f(50.f,  50.f,  50.f);
	    glVertex3f(50.f, -50.f,  50.f);

	    glVertex3f(-50.f, -50.f,  50.f);
	    glVertex3f(-50.f, -50.f, -50.f);
	    glVertex3f( 50.f, -50.f, -50.f);
	    glVertex3f( 50.f, -50.f,  50.f);

	    glVertex3f(-50.f, 50.f,  50.f);
	    glVertex3f(-50.f, 50.f, -50.f);
	    glVertex3f( 50.f, 50.f, -50.f);
	    glVertex3f( 50.f, 50.f,  50.f);

	glEnd();


	// Finally, display rendered frame on screen
	w->Display();

	return nextState;
};