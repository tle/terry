#include "MenuState.h"

string MenuState::run(RenderWindow *w) {
	cout << "I'm in a MenuState!" << endl;
	sf::Event Event;

	string nextState = "menu";


	while (w->GetEvent(Event))	{
	    // Close window : exit
	    if (Event.Type == sf::Event::Closed)
	        w->Close();
	    // Escape key : exit
	    if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::Escape))
	        w->Close();

	    // S key : switch states
	    if ((Event.Type == sf::Event::KeyPressed) && (Event.Key.Code == sf::Key::S))
	        nextState = "main";

	    // Resize event : adjust viewport
	    if (Event.Type == sf::Event::Resized)
	        glViewport(0, 0, Event.Size.Width, Event.Size.Height);
	}

	w->Clear();
	// Clear color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Declare and load a font
	sf::Font font;

	// Create a text
	sf::String text("main menu, press S to show a cube", sf::Font::GetDefaultFont(), 20);
	text.SetColor(sf::Color(200, 128, 0));
	text.SetPosition(200, 300);


	 // Draw it
	w->Draw(text);

	// Finally, display rendered frame on screen
	w->Display();

	return nextState;
};